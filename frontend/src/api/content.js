import request from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function getContentList(params) {
  return request({
    url: api_prefix + 'content/list',
    method: 'get',
    params
  })
}

export function getContentByID(id) {
  return request({
    url: api_prefix + 'content/get_id',
    method: 'get',
    params: { id }
  })
}

export function getContentChildrenCountByCategory(category_id) {
  return request({
    url: api_prefix + 'content/get_category_children',
    method: 'get',
    params: { category_id }
  })
}

export function getContentOperationUser(params) {
  return request({
    url: api_prefix + 'content/get_operation_user',
    method: 'get',
    params
  })
}

export function addContent(data) {
  return request({
    url: api_prefix + 'content/add',
    method: 'post',
    data
  })
}

export function editContent(data) {
  return request({
    url: api_prefix + 'content/edit',
    method: 'put',
    data
  })
}

export function updateContent(data) {
  return request({
    url: api_prefix + 'content/update',
    method: 'put',
    data
  })
}

export function deleteContent(ids) {
  return request({
    url: api_prefix + 'content/delete',
    method: 'delete',
    data: { ids }
  })
}
