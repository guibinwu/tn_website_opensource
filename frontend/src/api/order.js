import request from '@/utils/request'
import request_file from '@/utils/request-file'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function getOrderList(params) {
  return request({
    url: api_prefix + 'order/list',
    method: 'get',
    params
  })
}

export function getOrderByID(id) {
  return request({
    url: api_prefix + 'order/get_id',
    method: 'get',
    params: { id }
  })
}

export function shopOrderDelivery(data) {
  return request({
    url: api_prefix + 'order/shop_delivery',
    method: 'post',
    data
  })
}

export function shopVirtualProductOrderServe(data) {
  return request({
    url: api_prefix + 'order/shop_virtual_product_serve',
    method: 'post',
    data
  })
}

export function refreshShopOrderExpress(data) {
  return request({
    url: api_prefix + 'order/shop_refresh_express',
    method: 'put',
    data
  })
}

export function refundOrder(data) {
  return request({
    url: api_prefix + 'order/refund',
    method: 'post',
    data
  })
}

export function deleteOrder(ids) {
  return request({
    url: api_prefix + 'order/delete',
    method: 'delete',
    data: { ids }
  })
}

export function getAllOrderDataExcel(data) {
  return request_file({
    url: api_prefix + 'order/get_all_order_excel',
    method: 'post',
    data
  })
}

export function getDeliveryOrderDataExcel(data) {
  return request_file({
    url: api_prefix + 'order/get_delivery_order_excel',
    method: 'post',
    data
  })
}
