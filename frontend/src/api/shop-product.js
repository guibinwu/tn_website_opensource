import request from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function getProductList(params) {
  return request({
    url: api_prefix + 'shop_product/list',
    method: 'get',
    params
  })
}

export function getProductByID(id) {
  return request({
    url: api_prefix + 'shop_product/get_id',
    method: 'get',
    params: { id }
  })
}

export function getProductCountByCategoryID(category_id) {
  return request({
    url: api_prefix + 'shop_product/get_category_count',
    method: 'get',
    params: { category_id }
  })
}

export function getAllProductCount() {
  return request({
    url: api_prefix + 'shop_product/get_all_count',
    method: 'get'
  })
}

export function getAllProductTitle(params) {
  return request({
    url: api_prefix + 'shop_product/get_all_title',
    method: 'get',
    params
  })
}

export function getAllProductSpecsData(params) {
  return request({
    url: api_prefix + 'shop_product/get_all_product_specs?XDEBUG_SESSION_START=11995',
    method: 'get',
    params
  })
}

export function addProduct(data) {
  return request({
    url: api_prefix + 'shop_product/add',
    method: 'post',
    data
  })
}

export function editProduct(data) {
  return request({
    url: api_prefix + 'shop_product/edit',
    method: 'put',
    data
  })
}

export function updateProduct(data) {
  return request({
    url: api_prefix + 'shop_product/update',
    method: 'put',
    data
  })
}

export function deleteProduct(ids) {
  return request({
    url: api_prefix + 'shop_product/delete',
    method: 'delete',
    data: { ids }
  })
}
