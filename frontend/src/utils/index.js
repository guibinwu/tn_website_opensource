/**
 * Created by PanJiaChen on 16/11/18.
 */

var _toString = Object.prototype.toString

export const item_type_name = [
  '单行文本框',
  '单选按钮',
  '复选框',
  '下拉菜单',
  '文本域',
  '富文本',
  '单图片',
  '多图片',
  '附件',
  '整型',
  '浮点型'
]

export const wx_mp_product_status = [
  { label: '编辑中', value: 1 },
  { label: '审核中', value: 2 },
  { label: '审核失败', value: 3 },
  { label: '审核成功', value: 4 },
  { label: '上架', value: 5 },
  { label: '回收站', value: 6 },
  { label: '逻辑删除', value: 9 },
  { label: '自主下架', value: 11 },
  { label: '售磬下架', value: 12 },
  { label: '违规下架/风控系统下架', value: 13 }
]

export const wx_mp_order_status = [
  { label: '待付款', value: 10 },
  { label: '待发货', value: 20 },
  { label: '待收货', value: 30 },
  { label: '完成', value: 100 },
  { label: '订单取消', value: 200 },
  { label: '主动取消', value: 250 }
]

export const order_status = [
  {
    label: '已提交',
    value: 1
  },
  {
    label: '已付款',
    value: 2
  },
  {
    label: '付款失败',
    value: 3
  },
  {
    label: '关闭订单',
    value: 4
  },
  {
    label: '发起退款',
    value: 5
  },
  {
    label: '退款成功',
    value: 6
  },
  {
    label: '退款异常',
    value: 7
  },
  {
    label: '退款关闭',
    value: 8
  },
  {
    label: '已发货',
    value: 9
  },
  {
    label: '用户发起退款',
    value: 10
  },
  {
    label: '订单结束',
    value: 11
  },
  {
    label: '已提交未支付',
    value: 12
  },
  {
    label: '用户取消退款',
    value: 13
  },
  {
    label: '用户签收快递',
    value: 14
  }
]

export const lottery_type = [
  {
    label: '转盘',
    value: 1
  },
  {
    label: '九宫格',
    value: 2
  }
]

/**
 * Parse the time to string
 * @param {(Object|string|number)} time
 * @param {string} cFormat
 * @returns {string | null}
 */
export function parseTime(time, cFormat) {
  if (arguments.length === 0) {
    return null
  }
  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if ((typeof time === 'string') && (/^[0-9]+$/.test(time))) {
      time = parseInt(time)
    }
    if ((typeof time === 'number') && (time.toString().length === 10)) {
      time = time * 1000
    }
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const time_str = format.replace(/{([ymdhisa])+}/g, (result, key) => {
    const value = formatObj[key]
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') { return ['日', '一', '二', '三', '四', '五', '六'][value ] }
    return value.toString().padStart(2, '0')
  })
  return time_str
}

/**
 * @param {number} time
 * @param {string} option
 * @returns {string}
 */
export function formatTime(time, option) {
  if (('' + time).length === 10) {
    time = parseInt(time) * 1000
  } else {
    time = +time
  }
  const d = new Date(time)
  const now = Date.now()

  const diff = (now - d) / 1000

  if (diff < 30) {
    return '刚刚'
  } else if (diff < 3600) {
    // less 1 hour
    return Math.ceil(diff / 60) + '分钟前'
  } else if (diff < 3600 * 24) {
    return Math.ceil(diff / 3600) + '小时前'
  } else if (diff < 3600 * 24 * 2) {
    return '1天前'
  }
  if (option) {
    return parseTime(time, option)
  } else {
    return (
      d.getMonth() +
      1 +
      '月' +
      d.getDate() +
      '日' +
      d.getHours() +
      '时' +
      d.getMinutes() +
      '分'
    )
  }
}

/**
 * @param {string} url
 * @returns {Object}
 */
export function getQueryObject(url) {
  url = url == null ? window.location.href : url
  const search = url.substring(url.lastIndexOf('?') + 1)
  const obj = {}
  const reg = /([^?&=]+)=([^?&=]*)/g
  search.replace(reg, (rs, $1, $2) => {
    const name = decodeURIComponent($1)
    let val = decodeURIComponent($2)
    val = String(val)
    obj[name] = val
    return rs
  })
  return obj
}

/**
 * @param {string} url
 * @returns {Object}
 */
export function param2Obj(url) {
  const search = url.split('?')[1]
  if (!search) {
    return {}
  }
  return JSON.parse(
    '{"' +
      decodeURIComponent(search)
        .replace(/"/g, '\\"')
        .replace(/&/g, '","')
        .replace(/=/g, '":"')
        .replace(/\+/g, ' ') +
      '"}'
  )
}

/**
 * 高亮指定字符串
 * @param {string} key 需要进行处理的字符串
 * @param {string} value 待处理的字符串
 * @param {string} color 需要高亮的颜色
 */
export function wrapperKeyword(key, value, color = '#1890ff') {
  function highlight(v) {
    return `<span style="color: ${color}">${v}</span>`
  }
  if (!key) {
    return value
  } else {
    return value.replace(new RegExp(key, 'ig'), value => highlight(value))
  }
}

/* eslint-disable */
/**
 * 判断传入的数据是否为数组
 * @param {*} obj 
 */
export function isArray(obj) {
  return _toString.call(obj).slice(8, -1) === 'Array'
}

/**
 * 判断传入的数据是否为对象
 * @param {*} obj 
 */
export function isObject(obj) {
  return _toString.call(obj).slice(8, -1) === 'Object'
}

/**
 * 判断传入的数据是否为函数
 * @param {*} obj 
 */
export function isFunction(obj) {
  return _toString.call(obj).slice(8, -1) === 'Function'
}
