import { request } from '@/utils/request'

export function preAppreciateOrder(data) {
  return request({
    url: 'pay/pre_appreciate_order',
    method: 'post',
    data
  })
}

export function preTNShopOrder(data) {
  return request({
    url: 'pay/pre_tn_shop_order',
    method: 'post',
    data
  })
}

export function payTNShopOrder(data) {
  return request({
    url: 'pay/pay_tn_shop_order',
    method: 'post',
    data
  })
}

export function cancelOrder(data) {
  return request({
    url: 'pay/cancel_order',
    method: 'delete',
    data
  })
}

export function cancelTNShopOrder(data) {
  return request({
    url: 'pay/cancel_tn_shop_order',
    method: 'delete',
    data
  })
}
