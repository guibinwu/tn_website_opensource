<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-06
 * Time: 17:32
 */

namespace app\admin\model;


use think\model\Pivot;

class AuthRoleMenu extends Pivot
{
    protected $autoWriteTimestamp = false;
}