<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-26
 * Time: 17:03
 */

namespace app\admin\controller;


use app\admin\BaseController;
use app\common\model\Category as CategoryModel;

class Category extends BaseController
{
    /**
     * 获取栏目Table树形数据
     * @http get
     * @url /category/table_tree
     * @return \think\response\Json
     */
    public function getTableTree()
    {
        $params = $this->request->get();

        $data = CategoryModel::getTableTreeData($params);

        return tn_yes('获取栏目TableTree数据成功', ['data' => $data]);
    }

    /**
     * 获取全部顶级节点信息
     * @http get
     * @url /category/get_all_top
     * @return \think\response\Json
     */
    public function getTopNode()
    {
        $data = CategoryModel::getTopItemData();

        return tn_yes('获取栏目顶级父级成功', ['data' => $data]);
    }

    /**
     * 获取栏目全部栏目
     * @http get
     * @url /category/get_all_children
     * @return \think\response\Json
     */
    public function getAllNode()
    {
        $data = CategoryModel::getAllItemData();

        return tn_yes('获取栏目全部栏目成功', ['data' => $data]);
    }

    /**
     * 获取对应栏目下的子栏目数量
     * @http get
     * @url /category/get_children_count
     * @return \think\response\Json
     */
    public function getChildrenCount()
    {
        $pid = $this->request->get('pid', 0);

        $count = CategoryModel::getChildrenCount($pid);

        return tn_yes('获取栏目的子元素数量成功', ['count' => $count]);
    }

    /**
     * 根据id获取对应栏目信息
     * @http get
     * @url /category/get_id
     * @return \think\response\Json
     */
    public function getByID()
    {
        $id = $this->request->get('id');

        $data = CategoryModel::getDataWithID($id);

        return tn_yes('获取栏目信息成功', ['data' => $data]);
    }

    /**
     * 添加栏目信息
     * @http post
     * @url /category/add
     * @return \think\response\Json
     */
    public function addCategory()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['pid'=>0,'title','sort','status'=>0]);

        $result = CategoryModel::addCategory($data);

        if ($result) {
            $this->request->log_content = '添加栏目成功';
            return tn_yes('添加栏目成功');
        }else {
            $this->request->log_content = '添加栏目失败';
            return tn_no('添加栏目失败');
        }
    }

    /**
     * 编辑栏目信息
     * @http put
     * @url /category/edit
     * @return \think\response\Json
     */
    public function editCategory()
    {
        $this->checkPutUrl();

        $data = $this->request->put(['id','pid'=>0,'title','sort','status'=>0]);

        $result = CategoryModel::editCategory($data);

        if ($result) {
            $this->request->log_content = '编辑栏目成功';
            return tn_yes('编辑栏目成功');
        }else {
            $this->request->log_content = '编辑栏目失败';
            return tn_no('编辑栏目失败');
        }
    }

    /**
     * 更新栏目信息
     * @http put
     * @url /category/update
     * @return \think\response\Json
     */
    public function updateCategory()
    {
        $this->checkPutUrl();

        // 获取put的数据
        $data = $this->request->put();

        $this->checkUpdateValidate($data);

        $result = CategoryModel::updateInfo($data['id'],[
            $data['field'] => $data['value']
        ]);

        if ($result) {
            $this->request->log_content = '更新栏目成功';
            return tn_yes('更新栏目成功');
        }else {
            $this->request->log_content = '更新栏目失败';
            return tn_no('更新栏目失败');
        }
    }

    /**
     * 删除栏目信息
     * @http delete
     * @url /category/delete
     * @return \think\response\Json
     */
    public function deleteCategory()
    {
        $this->checkDeleteUrl();

        $ids = $this->request->delete('ids');

        $result = CategoryModel::delCategory($ids);

        if ($result) {
            $this->request->log_content = '删除栏目成功';
            return tn_yes('删除栏目成功');
        }else {
            $this->request->log_content = '删除栏目失败';
            return tn_no('删除栏目失败');
        }
    }
}