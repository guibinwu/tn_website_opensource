<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-14
 * Time: 21:37
 */

namespace app\admin\controller;


use app\admin\BaseController;
use app\admin\model\SystemLog as SystemLogModel;

class SystemLog extends BaseController
{
    /**
     * 获取日志列表信息
     * @http get
     * @url /system_log/list
     * @return \think\response\Json
     */
    public function getList()
    {
        $params = $this->request->param();

        $data = SystemLogModel::getPaginationList($params);

        return tn_yes('获取日志列表数据成功', return_vue_element_admin_pagination_data($data));
    }

    /**
     * 删除指定的日志信息
     * @http DELETE
     * @url /system_log/delete
     * @return \think\response\Json
     */
    public function deleteLog()
    {
        $this->checkDeleteUrl();

        $data = $this->request->delete('ids');

        $result = SystemLogModel::delByIDs($data);

        if ($result) {
            $this->request->log_content = '删除日志信息成功';
            return tn_yes('删除日志信息成功');
        }else {
            $this->request->log_content = '删除日志信息失败';
            return tn_no('删除日志信息失败');
        }
    }

    /**
     * 清空日志信息
     * @http delete
     * @url /system_log/clear_all
     * @return \think\response\Json
     */
    public function clearAllLog()
    {
        $this->checkDeleteUrl();

        $result = SystemLogModel::clearAllLog();

        if ($result) {
            $this->request->log_content = '清空日志信息成功';
            return tn_yes('清空日志信息成功');
        } else {
            $this->request->log_content = '清空日志信息失败';
            return tn_yes('清空日志信息失败');
        }
    }
}