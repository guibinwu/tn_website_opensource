<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-30
 * Time: 10:10
 */

namespace app\admin\middleware;


use app\common\exception\SuperAdminException;

class SuperAdmin
{
    public function handle($request, \Closure $next)
    {
        if ($request->param('id') == 1
            || (!is_array($request->param('ids')) && strpos($request->param('ids'),'1'))
            || (is_array($request->param('ids')) && in_array(1,$request->param('ids')))
        ) {
            throw new SuperAdminException();
        }

        return $next($request);
    }
}