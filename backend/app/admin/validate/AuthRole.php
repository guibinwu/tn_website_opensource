<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-15
 * Time: 08:32
 */

namespace app\admin\validate;


use app\common\validate\BaseValidate;

class AuthRole extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'title' => 'require|max:20',
        'name' => 'require|max:20',
        'status' => 'require|number|between:0,1'
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'title.require' => '权限角色标题不能为空',
        'title.max' => '权限角色标题最大长度不能超过20',
        'name.require' => '权限角色名称不能为空',
        'name.max' => '权限角色名称最大长度不能超过20',
        'status.require' => '状态不能为空',
        'status.number' => '状态值格式错误',
        'status.between' => '状态值必须在0，1之间',
    ];

    protected $scene = [
        'add' => ['title','name','status'],
        'edit' => ['id','title','name','status'],
    ];
}