<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-03
 * Time: 12:17
 */

namespace app\api\controller\mp\v1;


use app\api\BaseController;
use app\common\model\Business as BusinessModel;

class Business extends BaseController
{
    /**
     * 获取业务标题的数据
     * @http GET
     * @url /business/get_title/:limit
     * :limit 获取标题的数量
     * @return \think\response\Json
     */
    public function getBusinessTitle()
    {
        $limit = $this->request->param('limit','4');

        $data = BusinessModel::getBusinessTitle($limit);

        return tn_yes('获取业务标题数据成功', ['data' => $data]);
    }

    /**
     * 获取业务的详细数据
     * @http GET
     * @url /business/get_id/:id
     * :id 业务id
     * @param $id
     * @return \think\response\Json
     */
    public function getBusinessData()
    {
        $id = $this->request->param('id');

        $data = BusinessModel::getBusinessByID($id);

        return tn_yes('获取业务数据成功', ['data' => $data]);
    }

    /**
     * 更新咨询用户数据
     * @http post
     * @url /business/update/advisory/:id
     * :id 业务id
     * @return \think\response\Json
     */
    public function updateAdvisoryUser()
    {
        $this->checkPostUrl();

        $id = $this->request->param('id');

        $query_count = BusinessModel::addAdvisoryUser($id);

        return tn_yes('更新咨询用户成功', ['query_count' => $query_count]);
    }
}