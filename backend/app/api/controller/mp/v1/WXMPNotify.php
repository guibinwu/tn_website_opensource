<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-10-13
 * Time: 17:45
 */

namespace app\api\controller\mp\v1;


use app\api\BaseController;
use app\api\model\mp\v1\WXMPNotifyEvent;

// 接收微信小程序推送过来的消息
class WXMPNotify extends BaseController
{
    /**
     * 处理接收到的消息和事件
     */
    public function ReceiveMessagePush()
    {
        (new WXMPNotifyEvent())->HandleMessagePushEvent();
        echo 'success';
        exit();
    }

    /**
     * 校验接收消息推送的URL地址是否有效
     * @http get
     * @url /mp_notify/message_push
     */
    public function VerifyMessagePushService()
    {
        $status = (new WXMPNotifyEvent())->VerifyMessagePushSignature();

        if ($status) {
//            Log::record($this->request->get('echostr', ''),'error');
            echo $this->request->get('echostr', '');
            exit();
        }
    }
}