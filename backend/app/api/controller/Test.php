<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-10-20
 * Time: 18:46
 */

namespace app\api\controller;


use app\api\BaseController;
use think\facade\Queue;

class Test extends BaseController
{
    public function thinkQueue()
    {
        $params = $this->request->post('params',[]);

        /*创建新消息并推送到消息队列*/
        // 当前任务由哪个类负责处理
        $job_handler_classname = "app\api\job\TestThinkQueue";
        // 当前队列归属的队列名称
        $job_queue_name = "api_job_queue";
        // 当前任务所需的业务数据
        $job_data = ['ts' => time(), 'bizid' => uniqid(), 'params' => $params];
        // 将任务推送到消息队列等待对应的消费者去执行
        $is_pushed = Queue::push($job_handler_classname, $job_data, $job_queue_name);
        if ($is_pushed === false) {
            return tn_no('dismiss job queue went wrong');
        }

        return tn_yes('success');
    }
}