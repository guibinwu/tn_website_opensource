<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-22
 * Time: 10:35
 */

namespace app\common\validate;


class Atlas extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'category_id' => 'require|number',
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'category_id.require' => '图集所属栏目不能为空',
        'category_id.number' => '图集所属栏目格式不正确'
    ];

    protected $scene = [
        'change_category' => ['id','category_id']
    ];
}