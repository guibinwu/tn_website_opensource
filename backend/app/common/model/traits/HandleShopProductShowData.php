<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-11-02
 * Time: 09:36
 */

namespace app\common\model\traits;


use app\common\exception\ParameterException;
use app\common\exception\ProductException;
use app\common\validate\IDMustBeRequire;

trait HandleShopProductShowData
{
    /**
     * 根据id获取展示的数据
     * @param $id
     * @return array
     */
    public static function getShowDataByID($id)
    {
        $validate = new IDMustBeRequire();
        if (!$validate->check(['id' => $id])) {
            throw new ParameterException([
                'msg' => $validate->getError()
            ]);
        }

        // 根据id获取对应的数据
        $data = static::with(['property','specs' => function($query) {
            $query->with(['property']);
        }, 'parameters'])
            ->find($id);
        if (!$data) {
            throw new ProductException();
        }

        $product_data = $data->toArray();
        $product_data['single_sku'] = empty($product_data['property']) ? 1 : 0;
        $product_data['price'] = self::getMinMaxPrice($product_data['specs']);

        // 重组sku数数据
        $product_data['sku_data'] = self::generateSkuData($product_data);

        // 删除部分字段信息(待会进行字段重组)
        unset($product_data['property']);
        unset($product_data['specs']);


        return $product_data;
    }

    /**
     * 获取商品规格的最小值和最大值
     * @param $specs
     * @return array
     */
    private static function getMinMaxPrice($specs)
    {
        $specs_price = array_column($specs, 'selling_price');
        return [
            'min' => min($specs_price),
            'max' => max($specs_price)
        ];
    }

    /**
     * 根据商品数据生成
     * @param $product_data
     * @return array
     */
    private static function generateSkuData($product_data)
    {
        $property = $product_data['property'];
        $specs = $product_data['specs'];
        $product_id = $product_data['id'];
        $product_title = $product_data['title'];
        $product_thumb = $product_data['main_image']['prefix'][0]; // 取第1张轮播图作为预览缩虐图

        // 如果没有属性则直接返回单规格数据
        if (empty($property)) {
            $specs = $specs[0];
            return [
                'id' => $product_id,
                'name' => $product_title,
                'product_thumb' => $product_thumb,
                'property_list' => [
                    'name' => '默认',
                    'list' => [
                        'name' => '默认'
                    ],
                ],
                'specs_list' => [
                    'specs_id' => $specs['id'],
                    'product_id' => $product_id,
                    'virtual_product' => $product_data['virtual_product'],
                    'product_title' => $product_title,
                    'image' => $specs['product_image']['prefix'] ?: $product_thumb,
                    'original_price' => $specs['original_price'],
                    'selling_price' => $specs['selling_price'],
                    'property_name' => '默认',
                    'property_name_arr' => ['默认'],
                    'stock' => $specs['stock']
                ]
            ];
        }

        $property_list = [];
        $specs_list = [];
        $property_specs_data = array_column($specs, 'property');

        // 生成specs对应的property的所有值的可能性
        $property_specs_tree = [];
        foreach ($property_specs_data as $psd_item) {
            foreach ($psd_item as $psdc_item) {
                if (!isset($property_specs_tree[$psdc_item['property_id']])) {
                    $property_specs_tree[$psdc_item['property_id']] = [];
                }
                if (!in_array($psdc_item['value'], $property_specs_tree[$psdc_item['property_id']])) {
                    array_push($property_specs_tree[$psdc_item['property_id']], $psdc_item['value']);
                }
            }
        }

        // 生成属性列表数据
        foreach ($property as $p_item) {
            $list = [];
            foreach ($property_specs_tree[$p_item['id']] as $pst_item) {
                $list[] = [
                    'name' => $pst_item
                ];
            }
            $property_list[] = [
                'name' => $p_item['name'],
                'list' => $list
            ];
        }

        // 生成规格列表数据
        foreach ($specs as $s_item) {
            $property_name_arr = array_column($s_item['property'], 'value');

            $specs_list[] = [
                'specs_id' => $s_item['id'],
                'product_id' => $product_id,
                'virtual_product' => $product_data['virtual_product'],
                'product_title' => $product_title,
                'image' => $s_item['product_image']['prefix'] ?: $product_thumb,
                'original_price' => $s_item['original_price'],
                'selling_price' => $s_item['selling_price'],
                'property_name' => implode(',', $property_name_arr),
                'property_name_arr' => $property_name_arr,
                'stock' => $s_item['stock']
            ];
        }

        return [
            'id' => $product_id,
            'name' => $product_title,
            'product_thumb' => $product_thumb,
            'property_list' => $property_list,
            'specs_list' => $specs_list
        ];
    }
}