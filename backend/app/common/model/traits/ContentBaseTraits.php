<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-30
 * Time: 11:13
 */

namespace app\common\model\traits;


use app\common\exception\ContentException;
use app\common\model\Category as CategoryModel;
use app\admin\model\ModelTable as ModelTableModel;
use app\common\model\MpApiUserToken;
use think\facade\Db;

trait ContentBaseTraits
{
    protected static $array_data_type = [3,8,9];    //传过来的数据是以数组的形式

    /**
     * 处理前端传过来的数据
     * @param $data
     */
    private static function handleSubmitData(&$data)
    {
        if (isset($data['model_id']) || empty($data['model_id'])) {
            $data['table_data'] = [];
        }

        foreach ($data['fields_data'] as $item) {
            $en_name = $item['en_name'];
            $value = isset($item['value']) ? $item['value'] : '';

            // 针对不同的类型进行不同的处理
            switch ($item['type']) {
                case 3:
                case 8:
                case 9:
                    $data['table_data'][$en_name] = !empty($value) ? json_encode($value, JSON_UNESCAPED_UNICODE) : json_encode([], JSON_UNESCAPED_UNICODE);
                    break;
                case 5:
                case 6:
                    $data['table_data'][$en_name] = remove_xss($value);
                    break;
                default:
                    $data['table_data'][$en_name] = $value;
                    break;
            }
        }
    }

    /**
     * 得到需要删除的文件
     * @param $new_data
     * @param $old_data
     * @param bool $only_delete 是否只删除旧数据，不进行比较（用于切换模型）
     * @return array
     */
    private static function getNeedDeleteFile($new_data, $old_data, $only_delete = false)
    {

        if (empty($old_data)) {
            throw new \Exception('需要处理的数据为空');
        }

        $needDeleteFile = [];

        if ($only_delete) {
            foreach ($new_data as $item) {
                if (empty($old_data[$item['en_name']])) {
                    continue ;
                }
                switch ($item['type']) {
                    case 6:
                        $needDeleteFile = array_merge($needDeleteFile, self::getRichTextImage($old_data[$item['en_name']]));
                        break;
                    // 附件
                    case 9:
                        $delete_file = [];
                        foreach (json_decode($old_data[$item['en_name']], true) as $file_item) {
                            $delete_file[] = $file_item['url'];
                        }
                        $needDeleteFile = array_merge($needDeleteFile, $delete_file);
                        break;
                }
            }
        } else {
            foreach ($new_data as $item) {
                if (empty($old_data[$item['en_name']])) {
                    continue ;
                }
                switch ($item['type']) {
                    case 6:
                        $old_file_data = self::getRichTextImage($old_data[$item['en_name']]);
                        $new_file_data = self::getRichTextImage($item['value']);
                        $needDeleteFile = array_merge($needDeleteFile, self::getDifferentFileWithArray($old_file_data, $new_file_data));
                        break;
                }
            }
        }

        return $needDeleteFile;
    }

    /**
     * 根据内容的栏目类型获取对应子栏目的全部id
     * @param $categoryType
     * @param $errorMsg
     * @param $errorCode
     * @return array
     */
    private static function getAllCategoryIDByContentType($categoryType,$errorMsg,$errorCode)
    {
        // 获取对应栏目类型下的子栏目数据
        $categoryAllID = CategoryModel::where([['pid','=',$categoryType],['status','=',1]])
            ->column('id');

        if (empty($categoryAllID)) {
            throw new ContentException([
                'msg' => $errorMsg,
                'errorCode' => $errorCode,
            ]);
        }

        return $categoryAllID;
    }

    /**
     * 获取模型对应数据表中的数据(数据集的append)
     * @param $value
     * @param $data
     * @return array|\think\Model|null
     */
    protected static function getTableData($data, $field = [])
    {

        if (!isset($data['model_id']) || !isset($data['table_id']) || empty($data['model_id']) || empty($data['table_id'])) {
            return [];
        }

        // 根据对应的模型表和模型数据获取附加内容
        $tableName = ModelTableModel::getTableEnnameByID($data['model_id']);
        return Db::name($tableName)->where('id','=',$data['table_id'])
            ->field($field)
            ->find();
    }

    /**
     * 检查该点赞用户是否已经操作过
     * @param $id
     * @return bool
     */
    public static function checkLikeUser($id)
    {
        // 获取当前用户的uid
        $uid = MpApiUserToken::getCurrentUID();

        // 进行模型关联查询，查看是否查询到该id的用户
        $business = static::find($id);
        $likeUser = $business->contentLikeUser()
            ->where('user_id','=',$uid)
            ->find();

        // 判断是否为空
        if (!empty($likeUser)) {
            return false;
        } else {
            return true;
        }

    }

    /**
     * 检查该查看用户是否已经操作过
     * @param $id
     * @return bool
     */
    public static function checkViewUser($id)
    {
        // 获取当前用户的uid
        $uid = MpApiUserToken::getCurrentUID();

        // 进行模型关联查询，查看是否查询到该id的用户
        $business = static::find($id);
        $likeUser = $business->contentViewUser()
            ->where('user_id','=',$uid)
            ->find();

        // 判断是否为空
        if (!empty($likeUser)) {
            return false;
        } else {
            return true;
        }

    }

    /**
     * 检查该分享用户是否已经操作过
     * @param $id
     * @return bool
     */
    public static function checkShareUser($id)
    {
        // 获取当前用户的uid
        $uid = MpApiUserToken::getCurrentUID();

        // 进行模型关联查询，查看是否查询到该id的用户
        $business = static::find($id);
        $likeUser = $business->contentShareUser()
            ->where('user_id','=',$uid)
            ->find();

        // 判断是否为空
        if (!empty($likeUser)) {
            return false;
        } else {
            return true;
        }

    }

    /**
     * 更新点赞的用户信息
     * @param $id
     * @return bool
     */
    public static function updateLikeUser($id)
    {
        // 获取当前用户的uid
        $uid = MpApiUserToken::getCurrentUID();

        // 将当前用户的uid存到数据库中
        $static = static::find($id);

        $static->like_count++;
        $static->save();
        $static->contentLikeUser()->save([
            'user_id'=>$uid
        ]);

        return $static->like_count;
    }

    /**
     * 更新查看的用户信息
     * @param $id
     * @param bool $add_user
     * @return bool
     */
    public static function updateViewUser($id,$add_user = false)
    {

        // 将当前用户的uid存到数据库中
        $static = static::find($id);
        $static->view_count++;
        $static->save();
        if ($add_user == true) {
            // 获取当前用户的uid
            $uid = MpApiUserToken::getCurrentUID();
            $static->contentViewUser()->save([
                'user_id'=>$uid
            ]);
        }


        return $static->view_count;
    }

    /**
     * 更新分享的用户信息
     * @param $id
     * @param bool $add_user
     * @return bool
     */
    public static function updateShareUser($id,$add_user = false)
    {

        // 将当前用户的uid存到数据库中
        $static = static::find($id);
        $static->share_count++;
        $static->save();
        if ($add_user == true) {
            // 获取当前用户的uid
            $uid = MpApiUserToken::getCurrentUID();
            $static->contentShareUser()->save([
                'user_id'=>$uid
            ]);
        }

        return $static->share_count;
    }

    /**
     * 更新点赞的用户信息后需要返回的信息
     * @param $id
     * @return array
     */
    public static function getLikeUserAfterUpdate($id)
    {
        $data = static::with(['contentLikeUser.user'=>function($query){
            $query->limit(4)->order(['create_time'=>'DESC'])->field(['content_id','user_id']);
        }])
            ->where([['id','=',$id]])
            ->field(['id','like_count'])
            ->find();

        return !$data ? [] : $data->toArray();
    }

}