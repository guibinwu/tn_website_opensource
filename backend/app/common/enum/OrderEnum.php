<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-01-13
 * Time: 15:22
 */

namespace app\common\enum;


class OrderEnum
{
    // 创建订单
    const CREATE_ORDER = 1;

    // 支付成功
    const PAY_SUCCESS = 2;

    // 支付失败
    const PAY_ERROR = 3;

    // 关闭订单
    const CLOSE_ORDER = 4;

    // 发起退款
    const CREATE_REFUND = 5;

    // 退款成功
    const REFUND_SUCCESS = 6;

    // 退款异常
    const REFUND_CHANGE = 7;

    // 退款关闭
    const REFUND_CLOSE = 8;

    // 已发货
    const SHIP_ORDER = 9;

    // 用户发起退款
    const USER_CREATE_REFUND = 10;

    // 订单结束
    const ORDER_END = 11;

    // 订单已经提交，但是没有支付
    const CREATE_PAY_TIMEOUT = 12;

    // 用户取消退款
    const USER_CANCEL_REFUND = 13;

    // 快递已被用户签收
    const USER_SIGN_EXPRESS = 14;
}