<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-18
 * Time: 20:28
 */

namespace app\common\exception;


class UploadException extends BaseException
{
    public $code = 500;
    public $msg = '文件已存在，请重新选择';
    public $errorCode = 10002;
}