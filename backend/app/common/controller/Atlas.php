<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-23
 * Time: 09:15
 */

namespace app\common\controller;


use app\BaseController;
use app\common\model\Atlas as AtlasModel;
use app\common\model\Upload as UploadModel;

class Atlas extends BaseController
{
    /**
     * 获取图集列表数据
     * @http get
     * @url /atlas/list
     * @return \think\response\Json
     */
    public function getList()
    {

        $params = $this->request->get();

        $data = AtlasModel::getImageList($params);

        return tn_yes('获取图集列表数据成功', return_vue_element_admin_pagination_data($data));
    }

    /**
     * 上传图片到图集中
     * @http post
     * @url /atlas/upload
     * @return \think\response\Json
     */
    public function uploadImage()
    {
        $this->checkPostUrl();

        // 获取上传的文件
        $file = $this->request->file('file');
        // 获取上传文件对应的栏目
        $category_id = $this->request->post('category_id', 0);

        $result = UploadModel::uploadImgListImage($file, $category_id);

        return !empty($result) ? tn_yes('上传图集文件成功',['data' => $result]) : tn_no('上传图集文件失败');
    }

    /**
     * 删除图集中的图片
     * @http delete
     * @url /atlas/delete
     * @return \think\response\Json
     */
    public function deleteImage()
    {
        $this->checkDeleteUrl();

        $data = $this->request->delete('ids');

        $result = AtlasModel::delByIDs($data);

        return $result ? tn_yes('删除图集图片成功') : tn_no('删除图集图片失败');
    }
}