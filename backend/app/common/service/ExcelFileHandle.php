<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-10-14
 * Time: 10:03
 */

namespace app\common\service;

use \PhpOffice\PhpSpreadsheet\IOFactory;
// 读取和生成Excel文件
class ExcelFileHandle
{
    /**
     * 根据路径读取excel文件中的内容
     * @param $file_path
     * @return array
     */
    public static function readExcelDataByColumnAndRow($file_path, $end_column = 'A', $start_row = 1)
    {
        // 创建读操作（自动识别excel文件类型）
        $reader = IOFactory::createReaderForFile($file_path);
        // 设置为只读操作
        $reader->setReadDataOnly(true);
        // 打开文件、载入Excel表格
        $excel = $reader->load($file_path);

        // 获取活动工作簿
        $sheet = $excel->getActiveSheet();

        $data = [];

        foreach ($sheet->getRowIterator($start_row) as $row) {
            $tmp_data = [];
            foreach ($row->getCellIterator('A', $end_column) as $cell) {
                $tmp_data[] = $cell->getFormattedValue();
            }
            $data[$row->getRowIndex()] = $tmp_data;
        }

        return $data;
    }
}